import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';
import { PostService, PageContent } from 'src/app/services/post.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {

  public isShowOnModal: boolean;
  public title;
  public contentHTML;

  constructor(
    private notificationService: NotificationService,
    private postService: PostService) {
    this.notificationService.isShowOnModal.subscribe(async (isShowOnModal) => {
      await this._showOnModal(isShowOnModal);
    });
  }

  ngOnInit() { }

  public onClose(isShowOnNextTime) {
    this.notificationService.isShowOnModal.next(false);
    localStorage.setItem('isShowOnNextTime', `${isShowOnNextTime}`);
  }

  private async _showOnModal(isShowOnModal: boolean) {
   /*  const lastestNotification = await this.postService.getLastedPost(PageContent.HOME_INFORM, 1).pipe(first()).toPromise();
    let _isShowOnModal = isShowOnModal;
    if (lastestNotification.length) {
      if (this._isModalCanShow(lastestNotification[0])) {
        this.title = lastestNotification[0].title;
        this.contentHTML = lastestNotification[0].content_html;

        const currentDate = new Date().setHours(0, 0, 0, 0);
        localStorage.setItem('lastTimeShowOnModal', `${currentDate}`);

      } else {
        _isShowOnModal = false;
      }
    } else {
      _isShowOnModal = false;
    }
    this.isShowOnModal = _isShowOnModal; */
  }

  private _isModalCanShow(lastestNotification) {
    const currentDate = new Date().setHours(0, 0, 0, 0);
    const isShowOnNextTime = localStorage.getItem('isShowOnNextTime');
    const lastTimeShowOnModal = localStorage.getItem('lastTimeShowOnModal');
    if (lastTimeShowOnModal === `${currentDate}`) {
      if (isShowOnNextTime === `${false}`) {
        return false;
      }
      return true;
    } else {
      if (lastestNotification.startDate <= currentDate && currentDate <= lastestNotification.endDate) {
        return true;
      }
      return false;
    }
  }
}
