import { Component, OnInit } from '@angular/core';
// import { environment } from 'src/environments/environment';
// import { AngularFireAuth } from '@angular/fire/auth';
// import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-header-top',
  templateUrl: './header-top.component.html',
  styleUrls: ['./header-top.component.scss'],
})
export class HeaderTopComponent implements OnInit {
  loginLink = null;
  exchangeRate: any;
  isLoggedIn = false;
  userId: any;

  constructor(
    // private auth: AngularFireAuth,
    // private fs: AngularFirestore,
  ) {
/*     if (this.auth.auth.currentUser) {
      this.userId = this.auth.auth.currentUser.uid;
      if (this.userId) {
        this.isLoggedIn = true;
      }
    } */

  /*   this.getExchangeRate().subscribe((exchangeRate: any) => {
      this.exchangeRate = exchangeRate.value;
    }); */
  }

  ngOnInit() { }

  getExchangeRate() {
   // return this.fs.collection('PARAMETER_ORDER_DEFAULT').doc('qD2eyt3ZNdmQkXNNvayJ').valueChanges().pipe();
  }

  getCurrentUser(): any {
    return sessionStorage.getItem('user') || undefined;
  }

  onLogout() {
    sessionStorage.removeItem('user');
    this.isLoggedIn = false;
  }

}
