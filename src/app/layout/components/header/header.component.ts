import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DynamicScriptLoaderService } from 'src/app/services/dynamic-script-loader.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, AfterViewInit {
  searchSites: any[];
  searchProduct: FormGroup;
  public pushRightClass: string;

  constructor(
    private translate: TranslateService,
    private dynamicScriptLoader: DynamicScriptLoaderService,
    public router: Router,
    private apiService: ApiService,
    private formBuilder: FormBuilder) {

    this.router.events.subscribe(val => {
      if (
        val instanceof NavigationEnd &&
        window.innerWidth <= 992 &&
        this.isToggled()
      ) {
        this.toggleSidebar();
      }
    });

    this.searchSites = [
      {
        value: 'taobao',
        text: 'TAOBAO.COM',
        url: 'https://s.taobao.com/search?q='
      },
      {
        value: 'tmall',
        text: 'TMALL.COM',
        url: 'https://list.tmall.com/search_product.htm?q='
      },
      {
        value: '1688',
        text: '1688.COM',
        url: 'https://s.1688.com/selloffer/offer_search.htm?_input_charset=utf-8&button_click=top&earseDirect=false&n=y&keywords='
      }
    ];

    this.searchProduct = this.formBuilder.group({
      searchText: ['', Validators.required],
      searchSite: [this.searchSites[0], Validators.required]
    });
  }

  ngOnInit() {
    this.pushRightClass = 'push-right';
  }

  ngAfterViewInit() {
    this._loadScripts();
  }

  private _loadScripts() {
    this.dynamicScriptLoader.load('header').then(data => { }).catch(error => console.log(error));
  }

  isToggled(): boolean {
    const dom: Element = document.querySelector('body');
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle(this.pushRightClass);
  }

  rltAndLtr() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle('rtl');
  }

  onLoggedout() {
    localStorage.removeItem('isLoggedin');
  }

  changeLang(language: string) {
    this.translate.use(language);
  }

  async onSearch() {
    const key: string = this.searchProduct.controls.searchText.value;
    const site: any = this.searchProduct.controls.searchSite.value;
    console.log(key);
    this.apiService.post('translate', { key: key }).then((result: any) => {
      window.open(site.url + result.message);
    });
  }
}
